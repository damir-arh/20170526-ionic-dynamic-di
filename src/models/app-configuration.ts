import { Configuration } from "../modules/api/configuration";
import { Platform } from "ionic-angular";
import { Injectable } from "@angular/core";

@Injectable()
export class AppConfiguration extends Configuration {
  constructor(platform: Platform) {
    super();
    this.isMobile = () => platform.is("mobile");
  }
}
