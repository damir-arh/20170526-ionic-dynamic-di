import { HttpClient } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { MobileApi } from "./mobile-api";
import { Api } from "./api";
import { Configuration } from "./configuration";

export function apiFactory(http: HttpClient, config: Configuration) {
  if (config.isMobile && config.isMobile()) {
    return new MobileApi(http);
  } else {
    return new Api(http);
  }
}

@NgModule({
  providers: [
    { provide: Api, useFactory: apiFactory, deps: [HttpClient, Configuration] },
    Configuration
  ]
})
export class ApiModule {}
