import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Api } from "../api/api";

@Injectable()
export class MobileApi extends Api {
  constructor(public http: HttpClient) {
    super(http);
    console.log("For Mobile");
  }
}
