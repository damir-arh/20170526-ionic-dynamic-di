import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class Api {

  constructor(public http: HttpClient) {
    console.log('Hello Api Provider');
  }

}
